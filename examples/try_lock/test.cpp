#include <twist/test/test.hpp>
#include <twist/test/chrono.hpp>

#include <twist/test/util/race.hpp>

#include <twist/stdlike/atomic.hpp>
#include <twist/stdlike/thread.hpp>

#include <twist/util/spin_wait.hpp>

#include <chrono>

using twist::stdlike::atomic;
using twist::util::SpinWait;

////////////////////////////////////////////////////////////////////////////////

class SpinLock {
 public:
  void Lock() {
    SpinWait spin_wait;
    while (locked_.exchange(true)) {
      spin_wait();
    }
  }

  // Blocking =(
  bool TryLock() {
    // return !locked_.exchange(true);

    if (!locked_.load()) {
      Lock();
      return true;
    } else {
      return false;
    }
  }

  void Unlock() {
    locked_.store(false);
  }

 private:
  atomic<bool> locked_{false};
};


////////////////////////////////////////////////////////////////////////////////

using namespace std::chrono_literals;

TEST_SUITE(TwistExamples) {
  auto Now() {
    return twist::test::SteadyClock::now();
  }

  SIMPLE_TWIST_TEST(Tick) {
    auto start = Now();
    twist::stdlike::this_thread::yield();
    auto elapsed = Now() - start;
    std::cout << "Elapsed: " << elapsed.count() << "ns" << std::endl;
  }

  // Runs instantly with fibers execution backend
  SIMPLE_TWIST_TEST(SleepFor) {
    auto start = Now();
    twist::stdlike::this_thread::sleep_for(1s);
    auto elapsed = Now() - start;
    std::cout << "Elapsed: " << elapsed.count() << "ns" << std::endl;
  }

// Runs instantly with fibers execution backend
  TWIST_ITERATE_TEST(NonBlocking, 5s) {
    size_t cs = 0;
    SpinLock spin_lock;

    auto contender = [&]() {
      if (spin_lock.TryLock()) {
        twist::test::sleep_for(1s);
        ++cs;
        spin_lock.Unlock();
      }
    };

    twist::test::util::Race race;
    race.Add(contender);
    race.Add(contender);
    race.Run();

    ASSERT_TRUE(cs == 1);
  }

}

RUN_ALL_TESTS()
