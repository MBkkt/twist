#pragma once

#include <twist/ed/stdlike/mutex.hpp>

#include <optional>
#include <stack>

// Not lock-free
template <typename T>
class Stack {
 public:
  void Push(T value) {
    std::lock_guard guard(mutex_);
    items_.push(std::move(value));
  }

  std::optional<T> Pop() {
    std::lock_guard guard(mutex_);
    if (!items_.empty()) {
      T top(std::move(items_.top()));
      items_.pop();
      return top;
    } else {
      return std::nullopt;
    }
  }

 private:
  twist::ed::stdlike::mutex mutex_;
  std::stack<T> items_;
};
