#include <twist/test/test.hpp>
#include <twist/test/assert.hpp>

#include <twist/test/util/lockfree.hpp>
#include <twist/test/util/race.hpp>

#include <twist/rt/fault/adversary/lockfree.hpp>

#include "stack.hpp"

#include <iostream>

TEST_SUITE(TwistExamples) {

  // Report deadlock with fibers, hangs with threads
  TWIST_TEST_TL(LockFree, 5s) {
    twist::rt::fault::SetAdversary(twist::rt::fault::CreateLockFreeAdversary());

    twist::test::util::ReportProgressFor<Stack<int>> stack;

    twist::test::util::Race race;

    static const int kThreads = 4;

    for (int i = 0; i < kThreads; ++i) {
      race.Add([&, i]() {
        int value = i;
        while (twist::test::KeepRunning()) {
          twist::test::util::EnablePark guard;

          stack->Push(value);
          value = *stack->Pop();
        }
      });
    }

    race.Run();
  }
}

RUN_ALL_TESTS()
