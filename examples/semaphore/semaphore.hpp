#pragma once

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/mutex.hpp>
#include <twist/ed/stdlike/condition_variable.hpp>

#include <cstdlib>

// Poorly synchronized counting semaphore

class CountingSemaphore {
 public:
  explicit CountingSemaphore(size_t initial_tokens)
      : tokens_(initial_tokens) {
  }

  // -1
  void Acquire() {
    std::unique_lock lock(mutex_);
    while (tokens_.load() == 0) {
      has_tokens_.wait(lock);
    }
    --tokens_;
  }

  // +1
  void Release() {
    //std::lock_guard guard(mutex_);
    ++tokens_;
    has_tokens_.notify_one();
  }

 private:
  twist::ed::stdlike::atomic<size_t> tokens_;
  twist::ed::stdlike::mutex mutex_;
  twist::ed::stdlike::condition_variable has_tokens_;
};
