#include <twist/test/test.hpp>
#include <twist/test/runs.hpp>

#include <twist/test/util/race.hpp>
#include <twist/test/util/plate.hpp>

#include <twist/stdlike/atomic.hpp>

#include <chrono>
#include <iostream>

////////////////////////////////////////////////////////////////////////////////

class SpinLock {
 public:
  void Lock() {
    while (locked_.exchange(true)) {
      // Backoff
    }
  }

  void Unlock() {
    locked_.store(false);
  }

 private:
  twist::stdlike::atomic<bool> locked_{false};
};

////////////////////////////////////////////////////////////////////////////////

using namespace std::chrono_literals;

void TestSpinLock(size_t threads) {
  std::cout << "Threads: " << threads << std::endl;

  SpinLock spinlock;
  twist::test::util::Plate plate;

  twist::test::util::Race race;

  for (size_t i = 0; i < threads; ++i) {
    race.Add([&]() {
      while (twist::test::KeepRunning()) {
        spinlock.Lock();
        {
          // Critical section
          plate.Access();
        }
        spinlock.Unlock();
      }
    });
  }

  race.Run();

  std::cout << "Critical sections: " << plate.AccessCount() << std::endl;
}

TWIST_TEST_RUNS(SpinLock, TestSpinLock)->TimeLimit(3s)->Run(1)->Run(2)->Run(3);

RUN_ALL_TESTS()
