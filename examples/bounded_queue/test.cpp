#include <twist/test/test.hpp>
#include <twist/test/util/race.hpp>
#include <twist/test/util/checksum.hpp>

#include "bounded_queue.hpp"

#include <atomic>
#include <iostream>

TEST_SUITE(TwistExamples) {
  TWIST_TEST_TL(BlockingQueue, 3s) {
    static const size_t kProducers = 3;
    static const size_t kConsumers = 2;

    static const int kPoisonPill = -1;

    BoundedQueue<int> queue{2, /*notify_all=*/true};

    std::atomic<size_t> producers_left{kProducers};

    twist::test::util::CheckSum<int> checksum;
    std::atomic<size_t> puts{0};

    twist::test::util::Race race;

    // Producers

    for (size_t i = 0; i < kProducers; ++i) {
      race.Add([&]() {
        int next_value = 0;
        while (twist::test::KeepRunning()) {
          ++next_value;
          queue.Put(next_value);
          checksum.Produce(next_value);
          ++puts;
        }
        if (--producers_left == 0) {
          // Last producer
          for (size_t j = 0; j < kConsumers; ++j) {
            queue.Put(kPoisonPill);
          }
        }
      });
    }

    // Consumers

    for (size_t j = 0; j < kConsumers; ++j) {
      race.Add([&]() {
        while (true) {
          int value = queue.Get();
          if (value == kPoisonPill) {
            break;
          } else {
            checksum.Consume(value);
          }
        }
      });
    }

    race.Run();

    std::cout << "# Puts: " << puts.load() << std::endl;
    std::cout << "Checksum value: " << checksum.Value() << std::endl;

    ASSERT_TRUE(checksum.Validate());
  }
}

RUN_ALL_TESTS()
