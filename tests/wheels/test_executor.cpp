#include <twist/test/util/executor.hpp>

#include <wheels/test/test_framework.hpp>

#include <thread>

using namespace std::chrono_literals;

TEST_SUITE(ScopedExecutor) {
  SIMPLE_TEST(OneTask) {
    twist::test::util::Executor executor;

    executor.Submit([]() {
      std::this_thread::sleep_for(500ms);
    });
  }

  SIMPLE_TEST(ManyTasks) {
    twist::test::util::Executor executor;

    for (size_t i = 0; i < 10; ++i) {
      executor.Submit([]() {
        std::this_thread::sleep_for(500ms);
      });
    }
  }
}
