cmake_minimum_required(VERSION 3.14)
project(twist)

include(cmake/CompileOptions.cmake)
include(cmake/Platform.cmake)
include(cmake/Processor.cmake)
include(cmake/Sanitize.cmake)
include(cmake/ClangFormat.cmake)
include(cmake/ClangTidy.cmake)

option(TWIST_DEVELOPER "Twist development mode" OFF)
option(TWIST_TESTS "Enable twist tests" OFF)
option(TWIST_EXAMPLES "Enable twist examples" OFF)
option(TWIST_BENCHMARKS "Enable twist benchmarks" OFF)

option(TWIST_FIBERS "Fibers execution backend" OFF)
option(TWIST_PRINT_STACKS "Print stacks on deadlock" OFF)
option(TWIST_FAULTY "Enable fault injection" OFF)

add_subdirectory(third_party)

add_subdirectory(twist)

if(TWIST_TESTS OR TWIST_DEVELOPER)
    add_subdirectory(tests)
endif()

if(TWIST_EXAMPLES OR TWIST_DEVELOPER)
    add_subdirectory(examples)
endif()

if(TWIST_BENCHMARKS AND CMAKE_BUILD_TYPE MATCHES "Release")
    add_subdirectory(benchmarks)
endif()

if(TWIST_DEVELOPER)
    add_subdirectory(play)
endif()
