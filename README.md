# Twist

[Fault injection](https://en.wikipedia.org/wiki/Fault_injection) for concurrent data structures / synchronization primitives written in C++

## How to use

- `#include <atomic>` → `#include <twist/ed/stdlike/atomic.hpp>`
- `std::atomic<T>` → `twist::ed::stdlike::atomic<T>`

### Twist-ed spinlock

```cpp
#include <twist/ed/stdlike/atomic.hpp>

class SpinLock {
 public:
  void Lock() {
    while (locked_.exchange(true)) {  // <- Faults injected here
      ;  // Backoff
    }
  }

  void Unlock() {
    locked_.store(false);  // <- Faults injected here
  }
 private:
  // Twisted atomic - drop-in replacement for std::atomic
  twist::ed::stdlike::atomic<bool> locked_{false};
};
```

## Examples

- [Deadlock](/examples/deadlock)
- [Bounded queue](/examples/bounded_queue)  
- [Semaphore](/examples/semaphore)
- [Lock-Free](/examples/lockfree)
- [TryLock](/examples/try_lock)

## Standard library support

Namespace [`twist::ed::stdlike`](twist/ed/stdlike)
- `atomic`
- `thread`
- `mutex`
- `condition_variable`
- `this_thread`
  - `yield`
  - `sleep_for` 
  - `this_thread::get_id`
- `random_device`

## Extensions / Utilities

Namespace [`twist::ed::wait`](twist/ed/wait)
- `Wait` + `Wake{One,All}` – blocking wait
- `SpinWait` – spinning

Namespace [`twist::ed::lang`](twist/ed/lang):
- `ThreadLocal<T>` and `ThreadLocalPtr<T>`

## Layers

| Depth | Layer                                  | Description |
| --- |----------------------------------------| --- |
| 0 | [`stdlike`](twist/ed/stdlike)          | _Facade_ |
| 1 | [`fault`](twist/rt/fault)              |  _Fault injection_ |
| 2 | [`strand`](twist/rt/strand)            | _Virtual threads (strands)_ |
| 3 | OS threads / [`fiber`](twist/rt/fiber) | _Execution backend_ |

## Three  (almost) orthogonal dimensions

- Fault injection: off / on (`TWIST_FAULTY`)
- Execution backend: threads / [fibers](/twist/rt/fiber/runtime) (`TWIST_FIBERS`)
- Sanitizers: [Address](https://clang.llvm.org/docs/AddressSanitizer.html) / [Thread](https://clang.llvm.org/docs/ThreadSanitizer.html) / none (`ASAN` / `TSAN`)

![Twist](dims.png)

## Fiber execution backend
   
   - Fast context switches (5x as fast as context switch for OS threads)
   - Randomized run queue in scheduler and wait queues in mutexes / condition variables
   - Deterministic execution (deterministic time, randomness)
   - Time compression
   - Compatibility with Address/Thread sanitizers
   - Deadlock detection

## Build profiles

| Name | CMake options | Description |
| --- | --- | --- |
| `FaultyFibers` |  `-DTWIST_FAULTY=ON -DTWIST_FIBERS=ON -DTWIST_PRINT_STACKS=ON` | Fault injection + Fibers execution backend |
| `FaultyThreadsASan` | `-DTWIST_FAULTY=ON -DASAN=ON` | Fault injection + OS threads + Address sanitizer |
| `FaultyThreadsTSan` | `-DTWIST_FAULTY=ON -DTSAN=ON` | Fault injection + OS threads + Thread sanitizer |

## Dependencies

`binutils-dev` package required for `-DTWIST_PRINT_STACKS=ON`

## Research

- [A Randomized Scheduler with Probabilistic Guarantees of Finding Bugs](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/asplos277-pct.pdf)
- [A Practical Approach for Model Checking C/C++11 Code](http://plrg.eecs.uci.edu/publications/toplas16.pdf)
