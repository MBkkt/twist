#pragma once

/*
 * Drop-in replacement for std::thread
 *
 * Contents:
 *   namespace twist::ed::stdlike
 *     class thread
 *     namespace this_thread
 *       fun get_id
 *       fun yield
 *       fun sleep_for
 */

#if defined(TWIST_FAULTY)

#include <twist/rt/fault/stdlike/thread.hpp>

namespace twist::ed::stdlike {

using thread = rt::fault::FaultyThread;  // NOLINT

namespace this_thread = rt::fault::this_thread;

}  // namespace twist::ed::stdlike

#else

#include <twist/rt/fault/stdlike/thread.hpp>

namespace twist::ed::stdlike {

using ::std::thread;

namespace this_thread = ::std::this_thread;

}  // namespace twist::ed::stdlike

#endif
