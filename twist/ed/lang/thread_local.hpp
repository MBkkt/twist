#pragma once

/*
 * Contents
 *   namespace twist::ed::lang
 *     class ThreadLocalPtr<T>
 *     class ThreadLocal<T>
 */

#include <twist/rt/strand/thread_local.hpp>

namespace twist::ed::lang {

using rt::strand::ThreadLocal;
using rt::strand::ThreadLocalPtr;

}  // namespace twist::ed::lang
