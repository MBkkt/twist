#pragma once

/*
 * Contents:
 *   namespace twist::ed::wait
 *     class SpinWait
 */

#include <twist/single_core.hpp>

#if TWIST_SINGLE_CORE

#include <twist/rt/strand/stdlike/thread.hpp>

namespace twist::ed::wait {

struct [[nodiscard]] SpinWait {
  void Spin() {
    Yield();
  }

  void operator()() {
    Spin();
  }

  bool IsEnough() const {
    return true;
  }

 private:
  void Yield() {
    rt::strand::stdlike::this_thread::yield();
  }
};

}  // namespace twist::ed::wait

#else

#include <twist/rt/thread/spin_wait.hpp>

namespace twist::ed::wait {

using rt::thread::SpinWait;

}  // namespace twist::ed::wait

#endif
