#pragma once

/*
 * Replacement for std::atomic<T>::wait/notify
 * std::atomic<T>::wait/notify api & impl are fundamentally
 * broken in many ways
 *
 * Contents
 *   namespace twist::ed::wait
 *     fun Wait
 *     fun WaitTimed
 *     fun WakeOne
 *     fun WakeAll
 */

#if defined(TWIST_FAULTY)

#include <twist/rt/fault/wait/wait.hpp>

namespace twist::ed::wait {

using rt::fault::Wait;
using rt::fault::WaitTimed;
using rt::fault::WakeOne;
using rt::fault::WakeAll;

}  // namespace twist::ed::wait

#else

#include <twist/rt/thread/wait/wait.hpp>

namespace twist::ed::wait {

using rt::thread::Wait;
using rt::thread::WaitTimed;
using rt::thread::WakeOne;
using rt::thread::WakeAll;

}  // namespace twist::ed::wait

#endif
