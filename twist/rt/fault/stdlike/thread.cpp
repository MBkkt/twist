#include <twist/rt/fault/stdlike/thread.hpp>

// TODO: move to fault
#include <twist/test/util/affinity.hpp>

#include <twist/rt/fault/adversary/adversary.hpp>

namespace twist::rt {
namespace fault {

FaultyThread::FaultyThread(ThreadRoutine routine)
    : impl_([routine = std::move(routine)]() mutable {
        SetTestThreadAffinity();
        GetAdversary()->Enter();
        routine();
        GetAdversary()->Exit();
      }) {
}

}  // namespace fault
}  // namespace twist