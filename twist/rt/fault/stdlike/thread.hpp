#pragma once

#include <twist/rt/strand/stdlike/thread.hpp>

#include <wheels/support/function.hpp>

namespace twist::rt {
namespace fault {

//////////////////////////////////////////////////////////////////////

class FaultyThread {
  using ThreadRoutine = wheels::UniqueFunction<void()>;
 public:
  FaultyThread(ThreadRoutine routine);

  // NOLINTNEXTLINE
  void join() {
    impl_.join();
  }

  // NOLINTNEXTLINE
  bool joinable() const {
    return impl_.joinable();
  }

  // NOLINTNEXTLINE
  void detach() {
    impl_.detach();
  }

 private:
  strand::stdlike::thread impl_;
};

//////////////////////////////////////////////////////////////////////

namespace this_thread {

// No fault injection needed
using strand::stdlike::this_thread::get_id;
using strand::stdlike::this_thread::yield;
using strand::stdlike::this_thread::sleep_for;

}  // namespace this_thread

}  // namespace fault
}  // namespace twist::rt
