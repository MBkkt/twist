#ifndef FAULTY_CONDVAR_IMPL
#error "Internal implementation file"
#endif

#include <twist/rt/fault/adversary/inject_fault.hpp>
#include <twist/rt/fault/random/helpers.hpp>

#include <twist/rt/fiber/runtime/wait_queue.hpp>
#include <twist/rt/fiber/runtime/scheduler.hpp>

// Implementation for fibers (TWIST_FIBERS) execution backend

namespace twist::rt::fault {

using namespace twist::rt::fiber;

// Disable fiber preemption in current scope
struct PreemptionGuard {
  PreemptionGuard() {
    GetCurrentScheduler()->PreemptDisable(true);
  }

  ~PreemptionGuard() {
    GetCurrentScheduler()->PreemptDisable(false);
  }
};

class FaultyCondVar::Impl {
 public:
  void Wait(Lock& lock) {
    if (++wait_call_count_ % 13 == 0) {
      return;
    }

    InjectFault();
    {
      PreemptionGuard guard;
      lock.unlock();
    }
    waiters_.Park();
    InjectFault();
    lock.lock();
  }

  void NotifyOne() {
    InjectFault();
    waiters_.WakeOne();
    InjectFault();
  }

  void NotifyAll() {
    InjectFault();
    waiters_.WakeAll();
    InjectFault();
  }

 private:
  fiber::WaitQueue waiters_{"condvar"};
  size_t wait_call_count_{0};
};

}  // namespace twist::rt::fault
