#ifndef FAULTY_CONDVAR_IMPL
#error "Internal implementation file"
#endif

#include <twist/rt/fault/adversary/inject_fault.hpp>
#include <twist/rt/fault/random/helpers.hpp>

#include <wheels/intrusive/list.hpp>

#include <mutex>
#include <condition_variable>
#include <thread>

// Implementation for thread execution backend

namespace twist::rt::fault {

namespace detail {

class OneShotEvent {
 public:
  void Await() {
    std::unique_lock lock(mutex_);
    while (!fired_) {
      fired_cond_.wait(lock);
    }
  }

  void Fire() {
    std::lock_guard guard(mutex_);
    fired_ = true;
    fired_cond_.notify_all();
  }

 private:
  std::mutex mutex_;
  bool fired_;
  std::condition_variable fired_cond_;
};

}  // namespace detail

class FaultyCondVar::Impl {
 private:
  struct WaitNode : public wheels::IntrusiveListNode<WaitNode>,
                    public detail::OneShotEvent {
    void Wake() {
      Fire();
    }
  };

  using WaitQueue = wheels::IntrusiveList<WaitNode>;

 public:
  void Wait(Lock &lock) {
    ++wait_call_count_;

    // emulate spurious wakeups
    if (wait_call_count_ % 13 == 0) {
      return;  // instant wakeup
    }

    WaitImpl(lock);

    if (wait_call_count_ % 5 == 0) {
      // give lock to contender thread, try to provoke intercepted wakeup
      lock.unlock();
      std::this_thread::yield();
      lock.lock();
    }
  }

  // notify random thread from waiting queue

  void NotifyOne() {
    std::unique_lock<std::mutex> queue_lock(queue_mutex_);

    if (queue_.IsEmpty()) {
      return;
    }

    auto *wait_node = UnlinkRandomItem(queue_);

    queue_lock.unlock();

    wait_node->Wake();
  }

  // notify all waiting threads in random order

  void NotifyAll() {
    // grab whole waiting queue
    std::unique_lock<std::mutex> queue_lock(queue_mutex_);
    auto queue = std::move(queue_);
    queue_lock.unlock();

    auto wake_order = ShuffleToVector(queue);

    for (WaitNode *wait_node : wake_order) {
      wait_node->Wake();
    }
  }

 private:
  void WaitImpl(Lock& lock) {
    std::unique_lock<std::mutex> queue_lock(queue_mutex_);

    // release thread's lock
    lock.unlock();

    // allocate on stack!
    WaitNode wait_node;

    queue_.PushBack(&wait_node);

    queue_lock.unlock();

    // await signal from notify_one/notify_all
    wait_node.Await();

    // acquire thread's lock back
    lock.lock();
  }

 private:
  WaitQueue queue_;
  std::mutex queue_mutex_;

  // just size_t, we don't expect concurrent wait invocations
  size_t wait_call_count_{0};
};

}  // namespace twist::rt::fault
