#pragma once

#include <twist/rt/fault/adversary/adversary.hpp>

namespace twist::rt {
namespace fault {

IAdversaryPtr CreateNopAdversary();

}  // namespace fault
}  // namespace twist::rt
