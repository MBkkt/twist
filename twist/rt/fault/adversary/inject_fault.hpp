#pragma once

#include <twist/rt/fault/adversary/adversary.hpp>

namespace twist::rt {
namespace fault {

inline void InjectFault() {
  GetAdversary()->Fault();
}

}  // namespace fault
}  // namespace twist::rt

// Voluntarily inject fault (yield/sleep/park) into current thread

#if defined(TWIST_FAULTY)

#define TWIST_VOLUNTARY_FAULT() twist::fault::InjectFault();

#else

#define TWIST_VOLUNTARY_FAULT()

#endif
