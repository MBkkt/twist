#pragma once

#include <twist/rt/fault/adversary/adversary.hpp>
#include <twist/rt/fault/adversary/yielder.hpp>

#include <wheels/support/sync_output.hpp>

namespace twist::rt {
namespace fault {

/////////////////////////////////////////////////////////////////////

class YieldAdversary : public IAdversary {
 public:
  YieldAdversary(size_t yield_freq) : yielder_(yield_freq) {
  }

  // Per-test methods

  void Reset() override {
    yielder_.Reset();
  }

  void PrintReport() override {
    wheels::SyncCout() << "Context switches injected: " << yielder_.YieldCount()
                       << std::endl;
  }

  // Per-thread methods

  void Enter() override {
    // Do nothing
  }

  void Fault() override {
    yielder_.MaybeYield();
  }

  void ReportProgress() override {
    // Ignore lock-free algorithms
  }

  void Exit() override {
    // Do nothing
  }

 private:
  Yielder yielder_;
};

}  // namespace fault
}  // namespace twist::rt
