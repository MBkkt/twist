#include <twist/rt/fault/adversary/adversary.hpp>

#include <twist/rt/fault/adversary/yield.hpp>

#include <wheels/support/singleton.hpp>

#include <atomic>
#include <cstdint>

namespace twist::rt {
namespace fault {

/////////////////////////////////////////////////////////////////////

class Holder {
 public:
  Holder() : adversary_(CreateDefaultAdversary()) {
  }

  IAdversary* Get() {
    return adversary_.get();
  }

  void Set(IAdversaryPtr adversary) {
    adversary_ = std::move(adversary);
  }

 private:
  static IAdversaryPtr CreateDefaultAdversary() {
    return std::make_shared<YieldAdversary>(10);
  }

 private:
  IAdversaryPtr adversary_;
};

IAdversary* GetAdversary() {
  return Singleton<Holder>()->Get();
}

void SetAdversary(IAdversaryPtr adversary) {
  Singleton<Holder>()->Set(std::move(adversary));
}

void AccessAdversary() {
  (void)GetAdversary();
}

}  // namespace fault
}  // namespace twist::rt
