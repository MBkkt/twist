#pragma once

#include <twist/rt/fault/stdlike/atomic.hpp>

#include <twist/rt/strand/wait.hpp>

namespace twist::rt::fault {

inline void Wait(FaultyAtomic<uint32_t>* atomic, uint32_t old) {
  strand::Wait(atomic->Impl(), old);
}

inline void WaitTimed(FaultyAtomic<uint32_t>* atomic, uint32_t old, std::chrono::milliseconds timeout) {
  strand::WaitTimed(atomic->Impl(), old, timeout);
}

inline void WakeOne(FaultyAtomic<uint32_t>* atomic) {
  strand::WakeOne(atomic->Impl());
}

inline void WakeAll(FaultyAtomic<uint32_t>* atomic) {
  strand::WakeAll(atomic->Impl());
}

}  // namespace twist::rt::fault
