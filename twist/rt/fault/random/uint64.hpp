#pragma once

#include <cstdint>

namespace twist::rt::fault {

uint64_t RandomUInt64();

}  // namespace twist::rt::fault
