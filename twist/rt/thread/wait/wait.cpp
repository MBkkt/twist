#include <twist/rt/thread/wait/wait.hpp>

#include <twist/rt/thread/wait/platform/wait.hpp>

#include <wheels/support/panic.hpp>

namespace twist::rt::thread {

void WaitTimed(std::atomic<uint32_t>* /*atomic*/, uint32_t /*old*/, std::chrono::milliseconds /*timeout*/) {
  WHEELS_PANIC("WaitTimed not implemented");
}

}  // namespace twist::rt::thread
