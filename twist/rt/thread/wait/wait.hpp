#pragma once

#include <twist/rt/thread/wait/platform/wait.hpp>

#include <atomic>
#include <chrono>

namespace twist::rt::thread {

inline void Wait(std::atomic<uint32_t>* atomic, uint32_t old) {
  while (atomic->load() == old) {
    PlatformWait((uint32_t*)atomic, old);
  }
}

void WaitTimed(std::atomic<uint32_t>* atomic, uint32_t old, std::chrono::milliseconds timeout);

inline void WakeOne(std::atomic<uint32_t>* atomic) {
  PlatformWake((uint32_t*)atomic, 1);
}

inline void WakeAll(std::atomic<uint32_t>* atomic) {
  PlatformWake((uint32_t*)atomic, 100500);
}

}  // namespace twist::rt::thread
