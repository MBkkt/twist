#pragma once

#include <twist/rt/fiber/runtime/api.hpp>

#include <wheels/test/test_framework.hpp>

#define TWIST_FIBER_IMPL_TEST(name)                       \
  void FiberTestRoutine##name(void);                      \
  SIMPLE_TEST(name) {                                     \
    ::twist::rt::fiber::RunScheduler(FiberTestRoutine##name); \
  }                                                       \
  void FiberTestRoutine##name()
