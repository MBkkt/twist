#pragma once

#include <twist/rt/fiber/runtime/fiber.hpp>
#include <twist/rt/fiber/runtime/time.hpp>

#include <wheels/support/assert.hpp>

#include <queue>
#include <tuple>
#include <chrono>

namespace twist::rt {
namespace fiber {

class SleepQueue {
 public:
  using Clock = FiberSteadyClock;
  using TimePoint = Clock::time_point;

 public:
  void Put(Fiber* f, std::chrono::milliseconds delay);
  bool IsEmpty() const;
  Fiber* Poll(TimePoint now);
  TimePoint NextDeadLine() const;

 private:
  static TimePoint ToDeadLine(std::chrono::milliseconds delay);

 private:
  struct Entry {
    Fiber* fiber;
    TimePoint deadline;

    using OrderingKey = std::tuple<TimePoint, FiberId>;

    OrderingKey Key() const {
      return {deadline, fiber->Id()};
    }

    bool operator<(const Entry& other) const {
      return Key() > other.Key();
    }
  };

 private:
  std::priority_queue<Entry> sleepers_;
};

}  // namespace fiber
}  // namespace twist::rt
