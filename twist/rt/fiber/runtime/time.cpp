#include <twist/rt/fiber/runtime/time.hpp>

#include <twist/rt/fiber/runtime/scheduler.hpp>

#include <wheels/support/assert.hpp>

namespace twist::rt::fiber {

//////////////////////////////////////////////////////////////////////

void VirtualTime::AdvanceTo(time_point tp) {
  WHEELS_VERIFY(tp >= now_nanos_, "Time cannot move backward");
  now_nanos_ = tp;
}

//////////////////////////////////////////////////////////////////////

using time_point = VirtualTime::time_point;

time_point FiberSteadyClock::now() {
  return GetCurrentScheduler()->Now();
}

}  // namespace twist::rt::fiber
