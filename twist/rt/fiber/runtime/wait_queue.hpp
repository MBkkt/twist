#pragma once

#include <twist/rt/fiber/runtime/queue.hpp>

#include <string>

namespace twist::rt {
namespace fiber {

class WaitQueue {
 public:
  WaitQueue(const std::string& descr = "?");

  void Park();

  void WakeOne();
  void WakeAll();

 private:
  // For deadlock report
  const std::string descr_;

  FiberQueue waiters_;
};

}  // namespace fiber
}  // namespace twist::rt
