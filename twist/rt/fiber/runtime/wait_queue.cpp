#include <twist/rt/fiber/runtime/wait_queue.hpp>

#include <twist/rt/fiber/runtime/scheduler.hpp>

#if defined(TWIST_FAULTY)
#include <twist/rt/fault/random/helpers.hpp>
#endif

namespace twist::rt {
namespace fiber {

static inline void Suspend(std::string_view where) {
  GetCurrentScheduler()->Suspend(where);
}

static inline void Resume(Fiber* waiter) {
  GetCurrentScheduler()->Resume(waiter);
}

WaitQueue::WaitQueue(const std::string& descr) : descr_(descr) {
}

void WaitQueue::Park() {
  Fiber* caller = GetCurrentFiber();
  waiters_.PushBack(caller);
  Suspend(/*where=*/descr_);
}

void WaitQueue::WakeOne() {
  if (waiters_.IsEmpty()) {
    return;
  }
#if defined(TWIST_FAULTY)
  Fiber* f = waiters_.PopRandom();
#else
  Fiber* f = waiters_.PopFront();
#endif
  Resume(f);
}

void WaitQueue::WakeAll() {
  while (!waiters_.IsEmpty()) {
    Fiber* f = waiters_.PopFront();
    Resume(f);
  }
}

}  // namespace fiber
}  // namespace twist::rt
