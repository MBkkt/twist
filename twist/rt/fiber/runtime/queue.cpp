#include <twist/rt/fiber/runtime/queue.hpp>

#include <twist/rt/fiber/runtime/scheduler.hpp>

#if defined(TWIST_FIBERS_ARRQ)

namespace twist::rt {
namespace fiber {

void FiberQueue::PushBack(Fiber* f) {
  WHEELS_VERIFY(impl_.TryPushBack(f), "Twist thread limit reached: " << impl_.Size());
}

bool FiberQueue::IsEmpty() const {
  return impl_.IsEmpty();
}

Fiber* FiberQueue::PopFront() {
  if (impl_.IsEmpty()) {
    return nullptr;
  }
  return impl_.PopFrontUnsafe();
}

Fiber* FiberQueue::PopRandom() {
  size_t size = impl_.Size();

  if (size == 0) {
    return nullptr;
  }

  size_t index = GetCurrentScheduler()->GenerateRandomUInt64() % size;

  Fiber* f = impl_[index];

  std::swap(impl_[size - 1], impl_[index]);
  impl_.PopBackUnsafe();

  return f;
}

}  // fiber
}  // twist

#else

#include <wheels/intrusive/list.hpp>

#include <twist/rt/fault/random/helpers.hpp>

namespace twist::rt {
namespace fiber {

void FiberQueue::PushBack(Fiber* f) {
  impl_.PushBack(f);
}

bool FiberQueue::IsEmpty() const {
  return impl_.IsEmpty();
}

Fiber* FiberQueue::PopFront() {
  return impl_.PopFront();
}

Fiber* FiberQueue::PopRandom() {
  return fault::UnlinkRandomItem(impl_);
}

}  // namespace fiber
}  // namespace twist::rt

#endif
