#pragma once

#include <wheels/support/noncopyable.hpp>

#include <chrono>

namespace twist::rt::fiber {

//////////////////////////////////////////////////////////////////////

class VirtualTime : private wheels::NonCopyable {
 public:
  using rep = uint64_t;
  using period = std::nano;
  using duration = std::chrono::duration<rep, period>;
  using time_point = std::chrono::time_point<VirtualTime>;

 public:
  VirtualTime() {
    Reset();
  }

  void AdvanceBy(duration d) {
    now_nanos_ += d;
  }

  void AdvanceTo(time_point tp);

  void Reset() {
    now_nanos_ = time_point{duration{0}};
  }

  time_point Now() const {
    return now_nanos_;
  }

 private:
  time_point now_nanos_;
};

//////////////////////////////////////////////////////////////////////

class FiberSteadyClock {
 public:
  using rep = VirtualTime::rep;
  using period = VirtualTime::period;
  using duration = VirtualTime::duration;
  using time_point = VirtualTime::time_point;
  static const bool is_steady = true;

 public:
  static time_point now();
};

};  // namespace twist::rt::fiber
