#include <twist/rt/fiber/runtime/api.hpp>

#include <twist/rt/fiber/runtime/scheduler.hpp>

namespace twist::rt {
namespace fiber {

//////////////////////////////////////////////////////////////////////

void RunScheduler(FiberRoutine init) {
  Scheduler scheduler;
  scheduler.Run(std::move(init));
}

//////////////////////////////////////////////////////////////////////

void Spawn(FiberRoutine routine) {
  GetCurrentScheduler()->Spawn(std::move(routine));
}

void Yield() {
  GetCurrentScheduler()->Yield();
}

void SleepFor(std::chrono::milliseconds delay) {
  GetCurrentScheduler()->SleepFor(delay);
}

FiberId GetFiberId() {
  return GetCurrentFiber()->Id();
}

}  // namespace fiber
}  // namespace twist::rt
