#include <twist/rt/fiber/runtime/sleep_queue.hpp>

namespace twist::rt {
namespace fiber {

void SleepQueue::Put(Fiber* f, std::chrono::milliseconds delay) {
  sleepers_.push({f, ToDeadLine(delay)});
}

bool SleepQueue::IsEmpty() const {
  return sleepers_.empty();
}

Fiber* SleepQueue::Poll(TimePoint now) {
  if (IsEmpty()) {
    return nullptr;
  }
  Entry e = sleepers_.top();
  if (e.deadline <= now) {
    sleepers_.pop();
    return e.fiber;
  } else {
    return nullptr;
  }
}

SleepQueue::TimePoint SleepQueue::NextDeadLine() const {
  WHEELS_VERIFY(!IsEmpty(), "Sleep queue is empty");
  return sleepers_.top().deadline;
}

SleepQueue::TimePoint SleepQueue::ToDeadLine(std::chrono::milliseconds delay) {
  return Clock::now() + delay;
}

}  // namespace fiber
}  // namespace twist::rt
