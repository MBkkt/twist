#pragma once

#include <context/stack.hpp>

#include <wheels/support/size_literals.hpp>

#include <vector>

using namespace wheels::size_literals;

namespace twist::rt {
namespace fiber {

context::Stack AllocateStack();

void ReleaseStack(context::Stack stack);

void SetMinStackSize(size_t bytes);

}  // namespace fiber
}  // namespace twist::rt
