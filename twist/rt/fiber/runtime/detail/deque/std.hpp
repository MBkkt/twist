#pragma once

#include <deque>

namespace twist::rt::fiber {

namespace detail {

// NB: Size must be power of 2!
template <typename T>
class PtrStdDeque {
 public:
  size_t Size() const {
    return buf_.size();
  }

  bool IsEmpty() const {
    return buf_.empty();
  }

  bool IsFull() const {
    return false;
  }

  // Precondition: IsFull() == false
  void PushBackUnsafe(T* ptr) {
    buf_.push_back(ptr);
  }

  bool TryPushBack(T* ptr) {
    PushBackUnsafe(ptr);
    return true;
  }

  // Precondition: IsEmpty() == false
  T* PopFrontUnsafe() {
    T* front = buf_.front();
    buf_.pop_front();
    return front;
  }

  T* PopBackUnsafe() {
    T* back = buf_.back();
    buf_.pop_back();
    return back;
  }

  T* TryPopFront() {
    if (buf_.empty()) {
      return nullptr;
    }
    return PopFrontUnsafe();
  }

  T*& operator[](size_t index) {
    return buf_[index];
  }

  void Clear() {
    buf_.clear();
  }

 private:
  std::deque<T*> buf_;
};

}  // namespace detail

}  // namespace twist::rt::fiber
