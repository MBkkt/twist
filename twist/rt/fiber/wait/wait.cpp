#include <twist/rt/fiber/wait/wait.hpp>

#include <twist/rt/fiber/runtime/scheduler.hpp>

namespace twist::rt {
namespace fiber {

namespace {

WaitQueue& WaitQueueFor(void* addr) {
  return GetCurrentScheduler()->Futex(addr);
}

}  // namespace

void Wait(Atomic<uint32_t>* atomic, uint32_t old) {
  WaitQueue& wait_queue = WaitQueueFor(atomic);
  while (atomic->load() == old) {
    wait_queue.Park();
  }
}

void WaitTimed(Atomic<uint32_t>* /*atomic*/, uint32_t /*old*/, std::chrono::milliseconds /*timeout*/) {
  WHEELS_PANIC("WaitTimed not implemented");
}

void WakeOne(Atomic<uint32_t>* atomic) {
  WaitQueueFor(atomic).WakeOne();
}

void WakeAll(Atomic<uint32_t>* atomic) {
  WaitQueueFor(atomic).WakeAll();
}

}  // namespace fiber
}  // namespace twist::rt
