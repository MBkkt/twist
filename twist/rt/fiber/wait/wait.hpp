#pragma once

#include <twist/rt/fiber/stdlike/atomic.hpp>

#include <chrono>

namespace twist::rt::fiber {

void Wait(Atomic<uint32_t>* atomic, uint32_t old);
void WaitTimed(Atomic<uint32_t>* atomic, uint32_t old, std::chrono::milliseconds timeout);

void WakeOne(Atomic<uint32_t>* atomic);
void WakeAll(Atomic<uint32_t>* atomic);

}  // namespace twist::rt::fiber
