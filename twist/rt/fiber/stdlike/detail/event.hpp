#pragma once

#include <twist/rt/fiber/runtime/wait_queue.hpp>

namespace twist::rt {
namespace fiber {

class OneShotEvent {
 public:
  explicit OneShotEvent(std::string descr) : wait_queue_(descr) {
  }

  void Await() {
    if (!ready_) {
      wait_queue_.Park();
    }
  }

  void Signal() {
    ready_ = true;
    wait_queue_.WakeAll();
  }

 private:
  bool ready_{false};
  WaitQueue wait_queue_;
};

}  // namespace fiber
}  // namespace twist::rt
