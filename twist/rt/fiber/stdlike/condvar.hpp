#pragma once

#include <twist/rt/fiber/runtime/wait_queue.hpp>
#include <twist/rt/fiber/stdlike/mutex.hpp>

// std::unique_lock
#include <mutex>

namespace twist::rt {
namespace fiber {

class CondVar {
 public:
  void Wait(Mutex& mutex) {
    mutex.Unlock();
    wait_queue_.Park();
    mutex.Lock();
  }

  void NotifyOne() {
    wait_queue_.WakeOne();
  }

  void NotifyAll() {
    wait_queue_.WakeAll();
  }

  // std::condition_variable interface

  using Lock = std::unique_lock<Mutex>;

  void wait(Lock& lock) {  // NOLINT
    Wait(*lock.mutex());
  }

  template <typename Predicate>
  void wait(Lock& lock, Predicate predicate) {  // NOLINT
    while (!predicate()) {
      wait(lock);
    }
  }

  void notify_one() {  // NOLINT
    NotifyOne();
  }

  void notify_all() {  // NOLINT
    NotifyAll();
  }

 private:
  WaitQueue wait_queue_{"condvar"};
};

}  // namespace fiber
}  // namespace twist::rt
