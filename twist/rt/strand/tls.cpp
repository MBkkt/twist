#include <twist/rt/strand/tls.hpp>

#include <twist/rt/fiber/runtime/scheduler.hpp>

namespace twist::rt {
namespace strand {

// NB: destroyed before TLSManager
static thread_local ManagedTLS tls;

TLS& TLSManager::AccessTLS() {
#if defined(TWIST_FIBERS)
  return fiber::GetCurrentFiber()->AccessFLS();
#else
  return tls.Access();
#endif
}

}  // namespace strand
}  // namespace twist::rt
