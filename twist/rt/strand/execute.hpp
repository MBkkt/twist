#pragma once

#if defined(TWIST_FIBERS)

#include <twist/rt/fiber/runtime/api.hpp>

namespace twist::rt::strand {

template <typename F>
void Execute(F f) {
  twist::rt::fiber::RunScheduler([f]() {
    f();
  });
}

}  // namespace twist::rt::strand

#else

namespace twist::rt::strand {

template <typename F>
void Execute(F f) {
  f();
}

}  // namespace twist::rt::strand

#endif
