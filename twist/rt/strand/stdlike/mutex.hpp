#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/rt/fiber/stdlike/mutex.hpp>

namespace twist::rt::strand::stdlike {

using mutex = fiber::Mutex;  // NOLINT

}  // namespace twist::rt::strand::stdlike

#else

// native threads

#include <mutex>

namespace twist::rt::strand::stdlike {

using mutex = std::mutex;  // NOLINT

}  // namespace twist::rt::strand::stdlike

#endif
