#include <twist/rt/strand/stdlike/thread.hpp>

namespace twist::rt::strand::stdlike {

#if defined(TWIST_FIBERS)

const thread_id kInvalidThreadId = -1;

#else

const std::thread::id kInvalidThreadId{};

#endif

}  // namespace twist::rt::strand::stdlike
