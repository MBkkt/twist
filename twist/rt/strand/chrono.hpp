#pragma once

#if defined(TWIST_FIBERS)

#include <twist/rt/fiber/runtime/time.hpp>

namespace twist::rt::strand {

using SteadyClock = fiber::FiberSteadyClock;

}  // namespace twist::rt::strand

#else

#include <chrono>

namespace twist::rt::strand {

using SteadyClock = std::chrono::steady_clock;

}  // namespace twist::rt::strand

#endif