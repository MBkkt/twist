#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/rt/fiber/wait/wait.hpp>

namespace twist::rt::strand {

using fiber::Wait;
using fiber::WaitTimed;
using fiber::WakeOne;
using fiber::WakeAll;

}  // namespace twist::rt::strand

#else

// native threads

#include <twist/rt/thread/wait/wait.hpp>

namespace twist::rt::strand {

using thread::Wait;
using thread::WaitTimed;
using thread::WakeOne;
using thread::WakeAll;

}  // namespace twist::rt::strand:

#endif
