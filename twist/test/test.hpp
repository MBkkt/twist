#pragma once

#include <wheels/test/test_framework.hpp>
#include <wheels/test/iterate.hpp>

#include <twist/rt/fault/adversary/adversary.hpp>

#include <wheels/support/preprocessor.hpp>

#include <chrono>
#include <functional>
#include <vector>

namespace twist::test {

using TestRoutine = std::function<void()>;

void RunTestRoutine(TestRoutine test_routine);
void RunIteratedTestRoutine(TestRoutine test_routine);

bool KeepRunning();

}  // namespace twist::test

////////////////////////////////////////////////////////////////////////////////

#define SIMPLE_TWIST_TEST(name)                            \
  void TwistTestRoutine##name();                           \
  SIMPLE_TEST(name) {                                      \
    ::twist::test::RunTestRoutine(TwistTestRoutine##name); \
  }                                                        \
  void TwistTestRoutine##name()

////////////////////////////////////////////////////////////////////////////////

#define TWIST_TEST(name, options)                         \
  void TwistTestRoutine##name();                                    \
  TEST(name, options) {                                             \
    ::twist::test::RunTestRoutine(TwistTestRoutine##name);          \
  }                                                                 \
  void TwistTestRoutine##name()

////////////////////////////////////////////////////////////////////////////////

#define TWIST_TEST_TL(name, time_limit) \
  TWIST_TEST(name, ::wheels::test::TestOptions().TimeLimit(time_limit))

////////////////////////////////////////////////////////////////////////////////

#define TWIST_ITERATE_TEST(name, time_limit)                               \
  void TwistIteratedTestRoutine##name();                                   \
  ITERATE_TEST(name, time_limit) {                                         \
    ::twist::test::RunIteratedTestRoutine(TwistIteratedTestRoutine##name); \
  }                                                                        \
  void TwistIteratedTestRoutine##name()
