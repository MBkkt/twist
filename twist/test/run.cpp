#include <twist/test/run.hpp>

#if defined(TWIST_FAULTY)
#include <twist/rt/fault/adversary/adversary.hpp>
#endif

#if defined(TWIST_FIBERS)

#include <twist/rt/fiber/runtime/api.hpp>

namespace twist {

void Run(wheels::UniqueFunction<void()> main) {
  rt::fiber::RunScheduler(std::move(main));

#if defined(TWIST_FAULTY)
  rt::fault::GetAdversary()->PrintReport();
#endif
}

}  // namespace twist

#else

namespace twist {

void Run(wheels::UniqueFunction<void()> main) {
  main();

#if defined(TWIST_FAULTY)
  rt::fault::GetAdversary()->PrintReport();
#endif
}

}  // namespace twist

#endif
