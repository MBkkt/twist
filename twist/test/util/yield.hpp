#pragma once

#include <twist/rt/strand/stdlike/thread.hpp>

namespace twist::test {

inline void Yield() {
  strand::stdlike::this_thread::yield();
}

}  // namespace twist::test
