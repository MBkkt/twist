#pragma once

#include <wheels/test/test_framework.hpp>
#include <wheels/support/function.hpp>

#include <twist/rt/strand/stdlike/thread.hpp>
#include <twist/test/util/affinity.hpp>

#include <vector>

namespace twist::test::util {

using ThreadRoutine = wheels::UniqueFunction<void()>;

////////////////////////////////////////////////////////////////////////////////

namespace detail {
void RunThreadRoutine(ThreadRoutine routine);
}  // namespace detail

////////////////////////////////////////////////////////////////////////////////

class Executor {
 public:
  void Submit(ThreadRoutine routine);

  ~Executor() {
    Join();
  }

  void Join();

 private:
  std::vector<rt::strand::stdlike::thread> threads_;
  bool joined_{false};
};

////////////////////////////////////////////////////////////////////////////////

rt::strand::stdlike::thread RunThread(ThreadRoutine routine);

}  // namespace twist::test::util
